public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	public String toString(){
		return "------------------------------------------------------- \n Center card: " + this.centerCard + "\n Player card: " + this.playerCard + "\n-------------------------------------------------------";
	}
	public void dealCards(){
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	public int getNumberOfCards(){
		return this.drawPile.length();
	}
	public int calculatePoints(){
		int result = 0;
		if(this.centerCard.getSuit().equals(this.playerCard.getSuit())){
			result+= 2;
		}else if(this.centerCard.getVal() == this.playerCard.getVal()){
			result+= 4;
		}else{
			return -1;
		}
		return result;
	}
}
		