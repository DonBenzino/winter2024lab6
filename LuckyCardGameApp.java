import java.util.Scanner;
public class LuckyCardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome, what do you wish to do: Play game (1) or Other (2)?");
		if(Integer.parseInt(reader.nextLine()) == 1){
			game();
		}else{
			tester();
		}
		System.out.println("Run the Program Again? Y or N");
		if((reader.nextLine().toUpperCase()).equals("Y")){
			main(new String[]{"Hello"});
		}
	}
	
	public static void game(){
		GameManager manager = new GameManager();
		int totalPoints = 0;
		while(totalPoints<5 && manager.getNumberOfCards() != 0){
			System.out.println(manager);
			totalPoints += manager.calculatePoints();
			System.out.println("Total points: " + totalPoints + "\n Points altered by: " + manager.calculatePoints());
			manager.dealCards();
		}
		if(totalPoints>4){
			System.out.println("You win");
		}else{
			System.out.println("You lose");
		}
	}
	
	
	public static void tester(){
		Scanner reader = new Scanner(System.in);
		
		int draw = 0;
		
		Deck deck1 = new Deck();
		
		deck1.shuffle();
		
		System.out.println("Welcome, how many cards do you wish to draw?");
		draw = Integer.parseInt(reader.nextLine());
		
		while(draw == 0){
			System.out.println("Not a valid input, try again.");
			draw = Integer.parseInt(reader.nextLine());
		}
		
		System.out.println(deck1.length());
		
		for(int i = 0; i<draw; i++){
			deck1.drawTopCard();
		}
		
		System.out.println(deck1.length());
		
		deck1.shuffle();
		
		System.out.println(deck1);
	}
}